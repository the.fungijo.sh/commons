package sh.fungijo.commons.net;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Inet4Address;
import java.net.URL;
import java.util.LinkedList;
import java.util.function.Consumer;

/**
 * Internet utilities class.
 * 
 * @author fums
 */
public class InternetUtilities {

    private static final String[] IPV4CHECKURLS = {
            "http://checkip.amazonaws.com",
            "https://ipecho.net/plain",
            "https://myexternalip.com/raw",
            "https://ipv4.icanhazip.com/",
    };

    /**
     * Get this device's external IPv4 address from an external IP address web
     * service provider.
     * 
     * @param ioExceptionHandler An {@link Consumer} which processes any
     * {@link IOException}s which occur during querying any of the services.
     * @return The external IPv4 address as a {@link String}, or null if no
     * external IPv4 address could be obtained.
     */
    public static String getExternalIPv4Address(Consumer<IOException>
            ioExceptionHandler) {
        String ip = null;
        int urlIdx = 0;
        while (ip == null) {
            try {
                URL whatismyip = new URL(IPV4CHECKURLS[urlIdx]);
                BufferedReader in = new BufferedReader(new InputStreamReader(
                        whatismyip.openStream()));
                ip = in.readLine().trim();
            } catch (IOException e) {
                if (ioExceptionHandler == null) {
                    System.err.printf(
                            "Error checking external IPv4 address on %s%n",
                            IPV4CHECKURLS[urlIdx]);
                } else {
                    ioExceptionHandler.accept(e);
                }
            } finally {
                urlIdx++;
            }
            if (!ip.matches("\\d+\\.\\d+\\.\\d+\\.\\d+")) {
                ioExceptionHandler.accept(new IOException(String.format(
                        "%s returned by %s not an IPv4 address", ip,
                        IPV4CHECKURLS[urlIdx])));
                ip = null;
            }
        }
        return ip;
    }

//    /**
//     * Perform a traceroute to the given remote server. The steps are returned
//     * in sequence in a {@link String} array. This implementation uses an OS
//     * shell call to run the traceroute and processes the output depending on
//     * the host OS type.
//     *  
//     * @param address The address to trace to.
//     * @return The traceroute, or an empty array if none exists.
//     * @throws IOException If an exception occured during opening or reading
//     * the traceroute process.
//     */
//    public static String[] traceroute(Inet4Address address) throws IOException {
//        Process process;
//
//        if(System.getProperty("os.name").toLowerCase().trim().contains("win")) {
//            process = Runtime.getRuntime().exec("tracert " + address.getHostAddress());
//        } else {
//            process = Runtime.getRuntime().exec("traceroute " + address.getHostAddress());
//        }
//
//        // System dependent read and processing of traceroute
//        try {
//            process.waitFor();
//        } catch (InterruptedException e) {
//            while (process.isAlive());
//        }
//        String line = null;
//        LinkedList<String> trace = new LinkedList<String>();
//        BufferedReader reader = new BufferedReader(
//                new InputStreamReader(process.getInputStream()));
//        while ((line = reader.readLine()) != null) {
//            if(System.getProperty("os.name").toLowerCase().trim().contains("win")) {
//                if (line.matches(".*\\d+\\.\\d+\\.\\d+\\.\\d+.*") && !line.contains("Tracing route to")) {
//                    if (line.contains("[")) {
//                        trace.add(line.substring(line.indexOf("[") + 1, line.lastIndexOf("]")));
//                    } else {
//                        trace.add(line.split("ms")[3].trim());
//                    }
//                }
//            } else {
//                /*
//traceroute to 8.8.8.8 (8.8.8.8), 30 hops max, 60 byte packets
// 1  LAPTOP-34CJRPE6.mshome.net (172.25.80.1)  2.023 ms  0.774 ms  0.747 ms
// 2  192.168.2.254 (192.168.2.254)  1.911 ms  1.648 ms  3.450 ms
// 3  192.168.1.254 (192.168.1.254)  3.353 ms  3.385 ms  3.481 ms
// 4  45-26-8-1.lightspeed.tukrga.sbcglobal.net (45.26.8.1)  4.530 ms  4.825 ms  4.736 ms
// 5  107.212.169.16 (107.212.169.16)  4.882 ms  4.587 ms  4.983 ms
// 6  12.242.113.19 (12.242.113.19)  4.895 ms  4.732 ms  5.255 ms
// 7  12.255.10.8 (12.255.10.8)  4.698 ms  5.209 ms  6.634 ms
// 8  * * *
    // TODO : why multiple hops on the last line
// 9  dns.google (8.8.8.8)  6.289 ms  7.543 ms 209.85.246.230 (209.85.246.230)  7.443 ms
//                 */
//                
//            }
//        }
//        
//        String[] tracert = new String[trace.size()];
//        tracert = trace.toArray(tracert);
//        return tracert;
//    }

}
