package sh.fungijo.commons.collection;

/**
 * Indicates this collection supports slicing.
 * 
 * @author fungijo.sh
 *
 * @param <T> The type of the collection.
 */
public interface Sliceable<T> {

    /**
     * Return a copy of this collection, sliced from index {@code i} to index
     * {@code j}. The range taken is [i, j). Index {@code j} must be greater
     * than index {@code i}, and both must be valid indices as well.
     * 
     * @param i The index to start the slice at (inclusive).
     * @param j The index to end the slice at (exclusive).
     * @return A copy of the collection, sliced at the given indices.
     * @throws IndexOutOfBoundsException If either index is outside the bounds
     * of this collection.
     * @throws IllegalArgumentException If {@code i} >= {@code j}.
     */
    public T slice(int i, int j);

    /**
     * Return a copy of this collection, sliced from index {@code i} to the end
     * of the collection. The range taken is {@code [i, collection.length)}.
     * Index i must be valid ({@code i < length}).
     * 
     * @param i The index to start the slice at (inclusive).
     * @return A copy of the collection, sliced from the given index.
     * @throws IndexOutOfBoundsException If the index is outside the bounds of
     * this collection.
     */
    public T sliceFrom(int i);

    /**
     * Return a copy of this collection, sliced from the start of the
     * collection to index {@code j} (exclusive). The range taken is
     * {@code [0, j)}. Index j must be valid ({@code j <= length}).
     * 
     * @param j The index to end the slice at (exclusive).
     * @return A copy of the collection, sliced to the given index.
     * @throws IndexOutOfBoundsException If the index is outside the bounds of
     * this collection.
     */
    public T sliceTo(int j);

}
