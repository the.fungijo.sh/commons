package sh.fungijo.commons.collection;

/**
 * A simple class for a 2-tuple.
 * @author fungijo.sh
 *
 * @param <A> The first type of this {@link Twople}.
 * @param <B> The second type of this {@link Twople}.
 */
public class Twople<A, B> {

	private final A a;

	private final B b;

	/**
	 * Get a {@link Twople} of the two instances.
	 * 
	 * @param a The first instance.
	 * @param b The second instance.
	 * @return A {@link Twople} of the two instances.
	 */
	public static <A, B> Twople<A, B> of(A a, B b) {
		return new Twople<>(a, b);
	}

	/**
	 * Make a twople.
	 * 
	 * @param a The first part.
	 * @param b The second part.
	 */
	private Twople(A a, B b) {
		this.a = a;
		this.b = b;
	}

	/**
	 * Get the first element in this {@link Twople}.
	 * 
	 * @return The first element in this {@link Twople}.
	 */
	public A first() {
	    return a;
	}

    /**
     * Get the last element in this {@link Twople}.
     * 
     * @return The last element in this {@link Twople}.
     */
    public B last() {
        return b;
    }

    /**
     * Get the first element in this {@link Twople}.
     * 
     * @return The first element in this {@link Twople}.
     */
    public A a() {
        return a;
    }

    /**
     * Get the last element in this {@link Twople}.
     * 
     * @return The last element in this {@link Twople}.
     */
    public B b() {
        return b;
    }

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean equals(Object o) {
		if (!(o instanceof Twople)) {
			return false;
		}
		@SuppressWarnings("rawtypes")
		Twople t = (Twople) o;
		if ((a == null) != (t.a == null) || (b == null) != (t.b == null)) {
			return false;
		}
		return (a == null || a.equals(t.a)) && (b == null || b.equals(t.b));
	}

    /**
     * {@inheritDoc}
     */
    @Override
	public int hashCode() {
        int hc = 32771;
        hc = 31 * hc + (a == null ? 0 : a.hashCode());
        hc = 31 * hc + (b == null ? 0 : b.hashCode());
        return hc;
	}

    /**
     * {@inheritDoc}
     */
    @Override
	public String toString() {
		return String.format("(%s, %s)",
		        a == null ? "null" : a.toString(),
		        b == null ? "null" : b.toString()
		                );
	}

}
