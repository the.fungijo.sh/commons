package sh.fungijo.commons.util;

/**
 * Indicates a given class's {@code toString} method will produce a complete
 * description of an object and that a {@code parseString} method will be
 * provided by the class to parse such strings into objects.
 * 
 * @author fungijo.sh
 */
public interface Parsable<T> {

    /**
     * {@inheritDoc}
     * 
     * This class implements {@link Parsable}, indicating that it produces a
     * string which completely represents an instance, and that an instance
     * can be reconstructed from the {@link Parsable.parseString} method.
     * 
     * @return A complete string representation of the object.
     */
    @Override
    public String toString();

    /**
     * Parse a {@link String} produced by the {@code toString} method to
     * produce an instance of the class.
     * 
     * @return An instance of the class from its String representation.
     */
    public T parseString();

}
