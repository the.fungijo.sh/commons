package sh.fungijo.commons.util;

import java.io.IOException;

/**
 * Wrap an IOException as an error to be thrown from thread and lambda blocks
 * and caught by other code. 
 * 
 * @author fums
 */
public class WrappedIOException extends Error {

    /**
     * 
     */
    private static final long serialVersionUID = -2332585001802497109L;

    private final IOException ioe;

    public WrappedIOException(IOException ioe) {
        this.ioe = ioe;
    }

    public IOException getWrapped() {
        return ioe;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof WrappedIOException)) {
            return false;
        }
        return (ioe == null && ((WrappedIOException)obj).ioe == null)
                || (ioe != null && ioe.equals(((WrappedIOException)obj).ioe));
    }

    public int hashCode() {
        return ioe.hashCode() + 1;
    }

    public String toString() {
        return String.format("WrappedIOException[%s]", ioe.toString());
    }

}
