/**
 * Common library for josh's code.
 * 
 * @author jo.sh
 * @version 2022-12-13
 */
package sh.fungijo.commons;